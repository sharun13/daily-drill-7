const products = [{
    shampoo: {
        price: "$50",
        quantity: 4
    },
    "Hair-oil": {
        price: "$40",
        quantity: 2,
        sealed: true
    },
    comb: {
        price: "$12",
        quantity: 1
    },
    utensils: [
        {
            spoons: { quantity: 2, price: "$8" }
        }, {
            glasses: { quantity: 1, price: "$70", type: "fragile" }
        }, {
            cooker: { quantity: 4, price: "$900" }
        }
    ],
    watch: {
        price: "$800",
        quantity: 1,
        type: "fragile"
    }
}]

let productsArray = Object.entries(products[0]);

// items with price more than $65

let productsWithPriceMoreThan65 = productsArray.reduce((acc, curr) => {
    if (typeof (curr[1].price) === 'string') {
        curr[1].price = parseInt(curr[1].price
            .replace('$', ''));
        if (curr[1].price > 65) {
            acc.push(curr);
        }
    } else {
        let nestedItem = (curr.flat());
        nestedItem.shift();
        let finalItems = nestedItem.filter((item) => {
            let key = Object.keys(item);
            let price = parseInt((item[key].price)
                .replace('$', ''));
            if (price > 65) {
                return item;
            }
        });
        acc.push(finalItems);
    }
    return acc;
}, []);

console.log(productsWithPriceMoreThan65.flat());

// items with quantity ordered more than 1

let productsWithQtyMoreThan1 = productsArray.reduce((acc, curr) => {
    if (typeof (curr[1].quantity) === 'number') {
        if (curr[1].quantity > 1) {
            acc.push(curr);
        }
    } else {
        let nestedItem = (curr.flat());
        nestedItem.shift();
        let finalItems = nestedItem.filter((item) => {
            let key = Object.keys(item);
            let quantity = item[key].quantity;
            if (quantity > 1) {
                return item;
            }
        });
        acc.push(finalItems);
    }
    return acc;
}, []);

console.log(productsWithQtyMoreThan1.flat());

// items which are mentioned as fragile

let fragileProducts = productsArray.reduce((acc, curr) => {
    if (curr[1].type) {
        if (curr[1].type === 'fragile') {
            acc.push(curr);
        }
    }
    else if (Array.isArray(curr[1])) {
        let nestedItem = (curr.flat());
        nestedItem.shift();
        let finalItems = nestedItem.filter((item) => {
            let key = Object.keys(item);
            if (item[key].type) {
                if (item[key].type === 'fragile') {
                    return item;
                }
            }
        });
        acc.push(finalItems);
    }
    return acc;
}, []);

console.log(fragileProducts.flat());

